<?php
require_once("../../../vendor/autoload.php");
use App\Gender\Gender;
$objGender=new Gender();

$allData=$objGender->index("obj");
$serial=1;



?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../resource/assets/css/simple-sidebar.css">
    <script src="../../../resource/assets/js/jquery.min.js"></script>
    <script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resource/assets/css/form-elements.css">
    <link rel="stylesheet" href="../../../resource/assets/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="../../../resource/assets/ico/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../../../resource/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../../../resource/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../../../resource/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../../../resource/assets/ico/apple-touch-icon-57-precomposed.png">

    <style>
        /* Remove the navbar's default margin-bottom and rounded borders */
        .navbar {
            margin-bottom: 0;
            border-radius: 0;
        }

        /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
        .row.content {height: 450px}

        /* Set gray background color and 100% height */
        .sidenav {
            padding-top: 20px;
            background-color: #0f0f0f;
            height: 100%;
        }

        /* Set black background color, white text and some padding */
        footer {
            background-color: #555;
            color: white;
            padding: 15px;
        }

        /* On small screens, set height to 'auto' for sidenav and grid */
        @media screen and (max-width: 767px) {
            .sidenav {
                height: auto;
                padding: 15px;
            }
            .row.content {height:auto;}
        }
    </style>
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Logo</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav ">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Projects</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid text-left">
    <div class="row content">
        <div class="col-sm-2 sidenav ">
            <ul class="sidebar-nav">
                <li>
                    <a href="create.php">Create</a>
                </li>
                <li>
                    <a href="viewlist.php">View List</a>
                </li>
                <li>
                    <a href="trashed.php">Trash List</a>
                </li>

            </ul>
        </div>
        <div class="col-sm-8 text-left">
            <h1>Welcome</h1>
            <hr>
            
            <div class="row">

               <table class='table-bordered' align='center' style='table-layout: auto'>
                <th style='text-align: center'  height='40px' width='70px'>Serial</th>
                <th style='text-align: center'  height='40px' width='60px'>ID</th>
                <th style='text-align: center'  height='40px'>Name</th>
                <th style='text-align: center'  height='40px'>Gender</th>
                <th style='text-align: center'  height='40px'>Action</th>
                 <?php
                    foreach($allData as $oneData){
                        echo "<tr style='height: 20px'>";
                        echo"<td>".$serial."</td>";
                        echo"<td>".$oneData->id."</td>";
                        echo"<td>".$oneData->name."</td>";
                        echo"<td>".$oneData->gender."</td>";

                        echo"<td>";
                        echo"<a href='view.php?id=$oneData->id'> <button type='button' class='btn btn-info' style='margin-left: 4px'>View</button> </a>";
                        echo"<a href='edit.php?id=$oneData->id'> <button type='button' class='btn btn-primary'>Edit</button> </a>";
                        echo"<a href='delete.php?id=$oneData->id'> <button type='button' class='btn btn-danger'>Delete</button> </a>";
                        echo"<a href='trash.php?id=$oneData->id'> <button type='button' class='btn btn-danger'>Trash</button> </a>";
                        echo"</td>";
                        echo "<tr>";
                        $serial++;

                        }?>
                   </table>;

            </div>
        </div>
        <div class="col-sm-2 sidenav">
            <div class="well">
                <p>ADS</p>
            </div>
            <div class="well">
                <p>ADS</p>
            </div>
        </div>
    </div>
</div>

<footer class="container-fluid text-center">
    <p>Footer Text</p>
</footer>

</body>
</html>

<script>
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(1200).fadeOut();
</script>
