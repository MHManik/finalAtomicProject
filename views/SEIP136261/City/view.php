<?php
if(session_status()!=PHP_SESSION_ACTIVE) session_start();

require_once("../../../vendor/autoload.php");
use App\City\City;
use App\Utility\Utility;

$singleData = array();
$obj = new City();
$obj->setData($_GET);

$singleData = $obj->show();
if ($singleData == false) {
  Utility::redirect("index.php");
}
$_SESSION['title'] = "City";
require_once("../../../resource/inc/header.php");

?>

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>  <a href="#">City</a> <a href="../City/view.php" class="current">City views</a> </div>
    <h1>List (City)</h1>
  </div>




  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-map-marker"></i> </span>
            <h5>Details of City</h5>
          </div>
          <div class="widget-content nopadding">
            <div class="col-xs-9 col-sm-9 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >


              <div class="panel panel-info">
                <div class="panel-heading">
                  <h3 class="panel-title"><?php echo $singleData->user_name ?></h3>
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png" class="img-circle img-responsive"> </div>

                    <div class=" col-md-9 col-lg-9 ">
                      <table class="table table-user-information">
                        <tbody>
                        <tr>
                          <td>City Name:</td>
                          <td><?php echo $singleData->user_name ?></td>
                        </tr>
                        <tr>
                          <td>City:</td>
                          <td><?php echo $singleData->city ?></td>
                        </tr>
                        <tr>
                          <td>Country:</td>
                          <td><?php echo $singleData->country ?></td>
                        </tr>

                        <tr>

                        </tr>

                        </tbody>
                      </table>
                    </div><!--End of col-md-9 col-lg-9  class-->
                  </div><!--End of row class-->
                </div><!--End of panel-body class-->
              </div><!--End of panel panel-info class-->
            </div><!--End of Col-sm col-lg-3-5--- class-->
          </div><!--End of widget-content nopadding class-->
        </div><!--End of widget-box class-->
      </div> <!--End of span12 class-->
    </div><!--End of row-fluid class-->
  </div> <!--End of container-fluid class-->









</div> <!--End of content class-->
      <!--Footer-part-->
      <div class="row-fluid">
        <div id="footer" class="span12"> 2016 &copy; BITM. Brought to you by <a href="#">The BITM</a> </div>
      </div>
      <!--end-Footer-part-->
      <script src="../../../resource/js/jquery.min.js"></script>
      <script src="../../../resource/js/jquery.ui.custom.js"></script>
      <script src="../../../resource/js/bootstrap.min.js"></script>
      <script src="../../../resource/js/jquery.uniform.js"></script>
      <script src="../../../resource/js/select2.min.js"></script>
      <script src="../../../resource/js/jquery.dataTables.min.js"></script>
      <script src="../../../resource/js/matrix.js"></script>
      <script src="../../../resource/js/matrix.tables.js"></script>

</body>
</html>