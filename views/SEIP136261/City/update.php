<?php
if(session_status()!=PHP_SESSION_ACTIVE) session_start();
require_once ("../../../vendor/autoload.php");
use App\City\City;
use App\Utility\Utility;
$object = new City();
$object->setData($_GET);
$getAllData = $object->show();
if ($getAllData == false) {
    Utility::redirect("index.php");
}

/*
use App\Utility\Utility;
$on = new Utility();
$on->dd($getAllData);
*/

$_SESSION['title'] = "City";
require_once("../../../resource/inc/header.php");
?>

<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="index.php">City</a> <a href="#" class="current">Update</a> </div>
        <?php
        use App\Message\Message;
        echo Message::message();
        ?>
        <h1>Edit City</h1>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-map-marker"></i> </span>
                        <h5>Update City</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="edit.php" name="basic_validate" id="basic_validate" novalidate="novalidate">
                            <div class="control-group">
                                <label class="control-label">User Name</label>
                                <div class="controls">
                                    <input type="text" name="user_name" id="user_name" value="<?php echo $getAllData->user_name ?>" required="">
                                    <input type="hidden" name="id" id="id" value="<?php echo $getAllData->id ?>" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">City</label>
                                <div class="controls">
                                    <input type="text" name="city" value="<?php echo $getAllData->city ?>" id="city" required="">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Country</label>
                                <div class="controls">
                                    <input type="text" name="country" id="country" value="<?php echo $getAllData->country ?>" required="">
                                    <input type="hidden" name="id" id="id" value="<?php echo $getAllData->id ?>" />
                                </div>
                            </div>
                            
                            <div class="form-actions">
                                <input type="submit" value=" Update" name="update" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php
require_once("../../../resource/inc/header.php");
?>
