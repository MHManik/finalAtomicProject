<?php
require_once("../../../vendor/autoload.php");
use App\City\City;
use App\Utility\Utility;

$objCity = new City();
$objCity->setData($_POST);

if (array_key_exists("recoverMarked", $_POST)){
    $objCity->recoverMarked($_POST['marked']);
}else{
    Utility::redirect("index.php");
}

if (array_key_exists("deleteMarked", $_POST)){
    $objCity->deleteMarked($_POST['marked']);
}else{
    Utility::redirect("index.php");
}
if (array_key_exists("trashMarked", $_POST)){
    $objCity->trashMarked($_POST['marked']);
}else{
    Utility::redirect("index.php");
}

