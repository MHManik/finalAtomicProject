<!DOCTYPE html>
<html lang="en">
<head>
    <title>Atomic Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="./resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="./resource/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="./resource/assets/css/form-elements.css">
    <link rel="stylesheet" href="./resource/assets/css/style.css">
    <link rel="stylesheet" href="./resource/assets/css/hoverstyle.css">
    <script src="./resource/assets/js/jquery.min.js"></script>
    <script src="./resource/assets/bootstrap/js/bootstrap.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="./resource/assets/ico/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="./resource/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="./resource/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="./resource/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="./resource/assets/ico/apple-touch-icon-57-precomposed.png">

    <style>
        /* Remove the navbar's default margin-bottom and rounded borders */
        .navbar {
            margin-bottom: 0;
            border-radius: 0;
        }

        /* Add a gray background color and some padding to the footer */
        footer {
            background-color: #f2f2f2;
            padding: 25px;
        }
    </style>
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Portfolio</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Gallery</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="jumbotron">
    <div class="container text-center">
        <h1>My Atomic Project</h1>
        <p>Name-Mosharaf Hossain, Batch-36th ,ID-SEIP-136261 BITM</p>
    </div>
</div>

<div class="container-fluid bg-3 text-center">
    <h3>Project Name</h3><br>
    <div class="row">
        <div class="col-sm-3">
            <p style="font-weight: bold; font-size: x-large; font-family:'Book Antiqua' ">BookTitle</p>
            <div class="hovereffect">
                <img class="img-responsive" src="././resource/assets/img/projectImg/Book-Title.jpg" alt="" style="width:100%">
                <div class="overlay">
                    <h2>BookTitle</h2>
                    <a class="info" href="views/SEIP136261/BookTitle/home.php">link here</a>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <p style="font-weight: bold; font-size: x-large; font-family:'Book Antiqua' ">Birthday</p>
            <div class="hovereffect">
                <img class="img-responsive" src="././resource/assets/img/projectImg/images.jpg" alt="" style="width:100%">
                <div class="overlay">
                    <h2>Birthday</h2>
                    <a class="info" href="views/SEIP136261/Birthday/home.php">link here</a>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <p style="font-weight: bold; font-size: x-large; font-family:'Book Antiqua' ">City</p>
            <div class="hovereffect">
                <img class="img-responsive" src="././resource/assets/img/projectImg/city.jpg" alt="" style="width:100%">
                <div class="overlay">
                    <h2>My City</h2>
                    <a class="info" href="views/SEIP136261/City/home.php">link here</a>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <p style="font-weight: bold; font-size: x-large; font-family:'Book Antiqua' ">Email</p>
            <div class="hovereffect">
                <img class="img-responsive" src="././resource/assets/img/projectImg/download.jpg" alt="" style="width:100%">
                <div class="overlay">
                    <h2>E-Mail</h2>
                    <a class="info" href="views/SEIP136261/Email/home.php">link here</a>
                </div>
            </div>
        </div>
    </div>
</div>

<br><br>

<div class="container-fluid bg-3 text-center">
    <div class="row">
        <div class="col-sm-3">
            <p style="font-weight: bold; font-size: x-large; font-family:'Book Antiqua' ">Gender</p>
            <div class="hovereffect">
                <img class="img-responsive" src="././resource/assets/img/projectImg/Gender.jpg" alt="" style="width:100%">
                <div class="overlay">
                    <h2>Gender</h2>
                    <a class="info" href="views/SEIP136261/Gender/home.php">link here</a>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <p style="font-weight: bold; font-size: x-large; font-family:'Book Antiqua' ">Hobbies</p>
            <div class="hovereffect">
                <img class="img-responsive" src="././resource/assets/img/projectImg/Hobbies.jpg" alt="" style="width:100%">
                <div class="overlay">
                    <h2>Hobbies</h2>
                    <a class="info" href="views/SEIP136261/Hobbies/home.php">link here</a>
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <p style="font-weight: bold; font-size: x-large; font-family:'Book Antiqua' ">Profile Picture</p>
            <div class="hovereffect">
                <img class="img-responsive" src="././resource/assets/img/projectImg/profilepic.jpg" alt="" style="width:100%">
                <div class="overlay">
                    <h2>Profile Picture</h2>
                    <a class="info" href="views/SEIP136261/ProfilePicture/home.php">link here</a>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <p style="font-weight: bold; font-size: x-large; font-family:'Book Antiqua' ">Summary of Org.</p>
            <div class="hovereffect">
                <img class="img-responsive" src="././resource/assets/img/projectImg/Summary%20Organisation.jpg" alt="" style="width:100%">
                <div class="overlay">
                    <h2>Organisation</h2>
                    <a class="info" href="views/SEIP136261/SummaryOfOrganization/home.php">link here</a>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
<br><br>

<footer class="container-fluid text-center">
    <p>Footer Text</p>
</footer>

</body>
</html>