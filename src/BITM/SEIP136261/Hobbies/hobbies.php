<?php
namespace App\Hobbies;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;

class Hobbies extends DB{


    public $id;
    public $name="user";
    public $hobbies="";

    public function __construct()
    {

        parent::__construct();

    }

    public function setData($data=NULL){

        if(array_key_exists('id',$data)){
            $this->id =$data['id'];
        }
        if(array_key_exists('name',$data)){


            $this->name= $data['name'];

        }
        if(array_key_exists('hobbies',$data)){

            $this->hobbies = implode(',', $data['hobbies']);
        }

    }
    public function store(){
        $arrData = array($this->name,$this->hobbies);
        $sql = "INSERT INTO hobbies(name,hobbies) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);
        if($result)
            Message::message("Data has been inserted successfully! :)");
        else
            Message::message("Your Data does not inserted. :(");

        Utility::redirect('create.php');

    }

    public function index($fetchMode='ASSOC'){
        $sql="SELECT * from hobbies WHERE is_deleted='No'";
        $STH = $this->DBH->query($sql);



        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();
    public function view($fetchMode='ASSOC')
    {
        $sql = 'SELECT * from hobbies WHERE id=' . $this->id;
        $STH = $this->DBH->query($sql);
        $fetchMode = strtoupper($fetchMode);
        if (substr_count($fetchMode, 'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);


        $arrData = $STH->fetch();
        return $arrData;
    }
    public function trash(){
        $sql='UPDATE hobbies SET is_deleted=Now() WHERE id='.$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect('viewlist.php');

    }
    public function trashed($fetchMode='ASSOC'){
        $sql = "SELECT * from hobbies where is_deleted <> 'No' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashed();

    public function recover(){

        $sql = "Update hobbies SET is_deleted='No' where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('trashed.php');
    }
    public function delete(){
        $sql='DELETE from hobbies WHERE id='.$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect('viewlist.php');


    }
    public function update(){
        $arrData=array($this->name,$this->hobbies);
        $sql='UPDATE hobbies SET name=?, hobbies=? WHERE id='.$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute($arrData);

        Utility::redirect('viewlist.php');
    }

}