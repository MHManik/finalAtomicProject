<?php
namespace App\Gender;


use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;

class Gender extends DB{

    public $id;
    public $name="user";
    public $gender="";

    public function __construct()
    {

        parent::__construct();

    }
    public function setData($data=NULL){

        if(array_key_exists('id',$data)){
            $this->id =$data['id'];
        }
        if(array_key_exists('name',$data)){


            $this->name= $data['name'];

        }
        if(array_key_exists('gender',$data)){

            $this->gender = $data['gender'];

        }

    }
    public function store(){
        $arrData = array($this->name,$this->gender);
        $sql = "INSERT INTO gender(name,gender) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);
        if($result)
            Message::message("Data has been inserted successfully! :)");
        else
            Message::message("Your Data does not inserted. :(");

        Utility::redirect('create.php');

    }

    public function index($fetchMode='ASSOC'){
        $sql="SELECT * from gender WHERE is_deleted='No'";
        $STH = $this->DBH->query($sql);



        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();
    public function delete(){
        $sql='DELETE from gender WHERE id='.$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect('viewlist.php');


    }
    public function trash(){
        $sql='UPDATE gender SET is_deleted=Now() WHERE id='.$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect('viewlist.php');

    }
    public function trashed($fetchMode='ASSOC'){
        $sql = "SELECT * from gender where is_deleted <> 'No' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashed();
    public function recover(){

        $sql = "Update gender SET is_deleted='No' where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('trashed.php');

    }// end of recover();
    public function view($fetchMode='ASSOC')
    {
        $sql = 'SELECT * from gender WHERE id=' . $this->id;
        $STH = $this->DBH->query($sql);
        $fetchMode = strtoupper($fetchMode);
        if (substr_count($fetchMode, 'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);


        $arrData = $STH->fetch();
        return $arrData;
    }
    public function update(){
        //$sql = "UPDATE `gender` SET `user_name` = '".$this->user_name."', `gender_type` = '".$this->genderType." WHERE `gender`.`id` = '".$this->id."' ";
        $sql = "UPDATE gender SET name = '$this->name', gender = '$this->gender' WHERE id = '$this->id'";
        $query = $this->DBH->prepare($sql);
        $result = $query->execute();

        if ($result){
            Message::message("Data Updated Successfully !");
        }else{
            Message::message("Data Update Fail !");
        }
        Utility::redirect("index.php");
    }//End of Uddate() Method...


}