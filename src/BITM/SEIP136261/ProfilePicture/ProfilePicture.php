<?php
namespace App\ProfilePicture;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;

class ProfilePicture extends DB
{

    public $id;
    public $name;
    public $profile_picture_name;
    public $temp_location;

    public function __construct()
    {

        parent::__construct();

    }

    public function setData($data = NULL)
    {

        if (array_key_exists('name', $data)) {
            $this->name = $data['name'];
        }

         if (array_key_exists('profile_picture', $data)) {

             $this->profile_picture_name = $data['profile_picture']['name'];
             $this->temp_location = $data['profile_picture']['tmp_name'];


            }
        }


    public function store()
    {
        $arrData = array($this->name, $this->profile_picture_name);
        $query = "INSERT INTO profile_picture (name,profile_picture) VALUES (?,?)";
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrData);

        if ($result) {
            Message::message("Data Inserted Successfully!");
            //$uploads_dir = "../../../../resource/img/picture";
            $uploads_dir = "pic/";
            $target_file = $uploads_dir . $this->profile_picture_name;
            $move = move_uploaded_file($this->temp_location, $target_file);
            if ($move) {
                Message::message("File/Image Upaloaded Successfully");
            }

        } else {
            Message::message("Fail!");
        }

        Utility::redirect('create.php');

    }
    public function index($fetchMode='ASSOC'){
        $sql = "SELECT * from profile_picture";;
        $STH=$this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;
    }
    public function view(){

        $query = $this->DBH->prepare("SELECT * FROM profile_picture");
        $query->execute();
        return $query->fetchAll();

    }// End of the view() Method..
}
