<?php
namespace App\City;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class City extends DB{

    public $id;
    public $name;
    public $city;
    

    public function __construct(){
        parent::__construct();
        if (!isset($_SESSION)) session_start();
    }//End of __construct..

    public function setData($postVariableData = NULL){

        if (array_key_exists("id", $postVariableData)){
            $this->id          = $postVariableData['id'];
        }
        if (array_key_exists("name", $postVariableData)){
            $this->name   = $postVariableData['name'];
        }
        if (array_key_exists("city", $postVariableData)){
            $this->city     = $postVariableData['city'];
        }
        
    }// End of setData Method

    public function store(){
        $arrData=array($this->name,$this->city);
        $sql="insert into city(name,city) VALUES (?,?)";
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);
        if($result) {

            Message::Message("Data has been inserted successfully :)");
        }
        else{
            Message::Message("Failed! Data has not been inserted successfully :(");

        }
        Utility::redirect('create.php');
    }

    public function index($fetchMode='ASSOC'){
        $sql="SELECT * from city WHERE is_deleted='No'";
        $STH = $this->DBH->query($sql);



        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();
    public function trashed($fetchMode='ASSOC'){
        $sql = "SELECT * from city where is_deleted <> 'No' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashed();
    public function trash(){
        $sql='UPDATE city SET is_deleted=Now() WHERE id='.$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect('viewlist.php');

    }
    public function delete(){
        $sql='DELETE from city WHERE id='.$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect('viewlist.php');


    }
    public function recover(){

        $sql = "Update city SET is_deleted='No' where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('trashed.php');

    }// end of recover();
    public function view($fetchMode='ASSOC')
    {
        $sql = 'SELECT * from city WHERE id=' . $this->id;
        $STH = $this->DBH->query($sql);
        $fetchMode = strtoupper($fetchMode);
        if (substr_count($fetchMode, 'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);


        $arrData = $STH->fetch();
        return $arrData;
    }

}