<?php
namespace App\BookTitle;
use App\Message\Message;
use App\Model\database as DB;
use App\Utility\Utility;
use PDO;

class BookTitle extends DB{
    public  $id;
    public $bookTitle;
    public $authorName;

    public function __construct()
    {
        parent::__construct();
        if(!isset($_SESSION)) session_start();
    }

    public function setData($postVariableData=Null){
        if(array_key_exists("id",$postVariableData)){
            $this->id=$postVariableData['id'];
        }
        if(array_key_exists("book_title",$postVariableData)){
            $this->bookTitle=$postVariableData["book_title"];
        }
        if(array_key_exists("author_name",$postVariableData)){
            $this->authorName=$postVariableData['author_name'];
        }
    }

    public function store(){
        $arrData=array($this->bookTitle,$this->authorName);
        $sql="insert into book_title(book_title,author_name) VALUES (?,?)";
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);
        if($result) {

            Message::Message("Data has been inserted successfully :)");
        }
        else{
            Message::Message("Failed! Data has not been inserted successfully :(");

        }
        Utility::redirect('create.php');
    }

    public function index($fetchMode='ASSOC'){
        $sql="SELECT * from book_title WHERE is_deleted='No'";
        $STH = $this->DBH->query($sql);



        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();

    public function view($fetchMode='ASSOC')
    {
        $sql = 'SELECT * from book_title WHERE id=' . $this->id;
        $STH = $this->DBH->query($sql);
        if (substr_count($fetchMode, 'obj') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);


        $arrData = $STH->fetch();
        return $arrData;
    }
    public function update(){
        $arrData=array($this->bookTitle,$this->authorName);
        $sql='UPDATE book_title SET book_title=?, author_name=? WHERE id='.$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute($arrData);

        Utility::redirect('viewlist.php');
    }

    public function delete(){
        $sql='DELETE from book_title WHERE id='.$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect('viewlist.php');


    }
    public function trash(){
        $sql='UPDATE book_title SET is_deleted=Now() WHERE id='.$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect('viewlist.php');

    }
    public function trashed($fetchMode='ASSOC'){
        $sql = "SELECT * from book_title where is_deleted <> 'No' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashed();

    public function recover(){

        $sql = "Update book_title SET is_deleted='No' where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('trashed.php');

    }// end of recover();


}